import java.math.BigInteger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import javax.swing.JButton;

import client.view.StatusWindow;
import client.view.WorklistItem;
import client.view.ProgressItem;
import network.Sniffer;
import network.SnifferCallback;

import rsa.Factorizer;
import rsa.ProgressTracker;

public class CodeBreaker implements SnifferCallback {

    private final JPanel workList;
    private final JPanel progressList;
    
    private final JProgressBar mainProgressBar;
    private JButton JButton;

    // -----------------------------------------------------------------------
    
    private CodeBreaker() {
        StatusWindow w  = new StatusWindow();
        w.enableErrorChecks();
        workList        = w.getWorkList();
        progressList    = w.getProgressList();
        mainProgressBar = w.getProgressBar();
    }
    
    // -----------------------------------------------------------------------
    
    public static void main(String[] args) {

        /*
         * Most Swing operations (such as creating view elements) must be performed in
         * the Swing EDT (Event Dispatch Thread).
         * 
         * That's what SwingUtilities.invokeLater is for.
         */

        SwingUtilities.invokeLater(() -> {
            CodeBreaker codeBreaker = new CodeBreaker();
            new Sniffer(codeBreaker).start();
          
        });
        
    }

    // -----------------------------------------------------------------------

    /** Called by a Sniffer thread when an encrypted message is obtained. */
    @Override
    public void onMessageIntercepted(String message, BigInteger n) {
        System.out.println("message intercepted (N=" + n + ")...");
        
        SwingUtilities.invokeLater(() -> {
        	WorklistItem w = new WorklistItem(n, message);
        	ProgressItem p = new ProgressItem(n,message);
        	
        	ProgressTracker tracker = new Tracker(p.getProgressBar(), mainProgressBar);
        	ExecutorService pool = Executors.newFixedThreadPool(2);
        	
        	workList.add(w);
        	JButton cancel = new JButton("cancel");
        	JButton remove = new JButton("remove");
        	cancel.addActionListener(e->{
        		
        	});
        	remove.addActionListener(e->{
        		progressList.remove(p);
        		mainProgressBar.setValue(mainProgressBar.getValue()-1000000);
        		mainProgressBar.setMaximum(mainProgressBar.getMaximum()-1000000);
        	});
        	Runnable task = () -> {
        		try {
					String answer = Factorizer.crack(message, n, tracker);
					SwingUtilities.invokeLater(() -> {
						p.getTextArea().setText(answer);
					});
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
        		
        	};
        	
        	JButton breaka = new JButton("Break");
        	breaka.addActionListener(e->{
        		progressList.add(p);
        		workList.remove(w);
        		pool.submit(task);
        	});
        	w.add(breaka);
        	p.add(cancel);
        	p.add(remove);
        });
    }
    private static class Tracker implements ProgressTracker {
    	private int totalprog = 0;
    	private JProgressBar progbar;
    	private JProgressBar mainprogbar;
    	public Tracker(JProgressBar progbar, JProgressBar mainprogbar) {
    		this.progbar = progbar;
    		this.mainprogbar = mainprogbar;
    	}
		@Override
		public void onProgress(int ppmDelta) {
			totalprog += ppmDelta;
			SwingUtilities.invokeLater(() -> {
    			int currentMain = mainprogbar.getValue();
    			progbar.setValue(totalprog);
        		mainprogbar.setValue(currentMain + ppmDelta);
    		});
			
			
		}
    	
    }
}
