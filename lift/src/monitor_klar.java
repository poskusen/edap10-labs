import java.util.Stack;

import lift.LiftView;
import lift.Passenger;

public class monitor_klar {
	private int floor;
	private boolean moving;
	private int direction;
	private int[] waitEntry;
	private int[] waitExit;
	private int load;
	private LiftView hiss;
	private boolean door;
	private Stack<Integer> movement;
	
	public monitor_klar(LiftView hiss) {
		this.hiss = hiss;
		this.floor = 0;
		this.moving = false; // is the hiss moving
		this.direction = 0; //not moving
		this.waitEntry = new int[]{0, 0, 0, 0, 0, 0, 0};
		this.waitExit = new int[]{0, 0, 0, 0, 0, 0, 0};
		this.load = 0;
		this.door = false;
		this.movement = new Stack<Integer>(); //track moving people

	}
	
	public synchronized void enter_start(int level_begin, int level_end) throws InterruptedException {
		this.waitEntry[level_begin]++; // waiting for entry
		notifyAll();
		while ((level_begin != this.floor) || this.moving || load > 3 || !this.door) {
			wait();
		}
		this.load++; // take load
		this.movement.add(0); // is moving
		
	}
	public synchronized void enter_end(int level_begin, int level_end) {
		this.waitExit[level_end]++; // waiting for exit
		this.waitEntry[level_begin]--; // entered
		this.movement.pop(); // no longer moving
		notifyAll(); 
	}
	public synchronized void exit_start(int level_end) throws InterruptedException {
		while (level_end != this.floor || this.moving || !this.door) {
			wait();
		}
		this.movement.add(0); // is moving
		this.load--; // remove load
		notifyAll();
	}
	public synchronized void exit_end(int level_end) throws InterruptedException {
		this.waitExit[level_end]--; // no longer waiting for exit
		this.movement.pop(); // no longer moving
		notifyAll();
	}
	public synchronized void move_start(int level) throws Exception {
		while ((((waitEntry[this.floor] != 0 & this.load < 4) || waitExit[this.floor] != 0)||(!someone_to_pick()))||!this.movement.isEmpty()) {
			wait(); // wait until it is allowed to move
		}
		if (level - floor == 0) {
			throw new Exception("F�rs�ker �ka till samma niv�");
		}
		if (this.door) { 
			close(); //close if not closed
		}
		this.direction = Math.abs(level - floor)/(level - floor);
		this.moving = true;
	}
	private boolean someone_to_pick() { // is there someone to pick up or leave off
		boolean someone = false;
		for (int i = 0; i < 7; i++) {
			if (waitEntry[i] != 0 || waitExit[i] != 0) {
				someone = true;
				break;
			}
		}
		
		return someone;
	}
	public synchronized void move_end(int level) throws Exception{
		this.moving = false;
		this.floor = level;
		if ((waitEntry[level] > 0 & load < 4)|| waitExit[level] > 0 ) {
			open(); // open if necese
		}
		notifyAll();
	}
	public LiftView get_hiss() {
		return hiss;
	}
	public synchronized void open() {
		this.door = true;
		hiss.openDoors(this.floor);
	}
	public synchronized void close() {
		this.door = false;
		hiss.closeDoors();
	}
}
